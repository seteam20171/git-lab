program master;
  Uses crt;
  Const
    count = 17;
    min = -10;
  Type
    Vector = array[1..count] of integer;
    Matrix = array[1..6, 1..6] of integer;
  Var
    DataArray:Vector;
    DataMatrix:Matrix;


begin
  Randomize;
  CreateArray(DataArray);
  Writeln('Array');
  PrintArray(DataArray);
  FindTranslocationZero(DataArray);
  Writeln('Array after moving zeros');
  PrintArray(DataArray);
  TranslocationMax(DataArray);
  Writeln('Array after moving max');
  PrintArray(DataArray);
  CreateMatrix(DataMatrix);
  Writeln('Matrix');
  PrintMatrix(DataMatrix);
  Writeln;
  FindMaxMainDiagonal(DataMatrix);
  Repeat until keypressed;
end.



